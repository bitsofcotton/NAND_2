# NAND(Never finish)
Natural simpler compiler which code with natural simpler comments and licenses on a file.
Aims to add one of a code stack s.t. small code amount on both compiler/library with comment connected (ideally one can read whole easily) bases on not the latest but a modern C compiler. This repository pefer explicit description.

<strike>We trust randtools optimizer strong, so we focus only the simple enough implementation even including infinite loop.</strike>
<strike>We cannot use randtools optimizer because the pigeon hole slips, So we reduce overhead as hard as possible with no loop order hard optimization.</strike>
A randtools optimizer is valid in making invariant meaning, however is not valid in output complexity meaning. So if we decide to use safe method, the latter decision, if we decide to believe invariant meaning strong, the former decision.

We aim the library as maximum compressed structures (module cohesion) for standard library as many languages do.

This aims to compile nand source into C source and check the output by hand or eyes. Otherwise, they can even infect if whole system is infected condition. (All we can do is to transcode and check the transformed or compiled source by hands or eyes even any of the code patches, or, we can do only to crypt them.)

# XXX (nota bene):
This isn't breaks the original system infection if compiler or interpreter or system are infected condition.
So to break them, we should read/write all of the raw binary on boot strap in as possibe as raw layer enough, or, only write the boot strap with some deep stacked VMs they cannot tracked with cryption.
However, latter one is hard to prove they really worked as expected because we cannot exclude completely the condition some of the big brother observing us with some of the low layer.
Also, the source code this repository have is observed from some another since before you download this repository, so if the infectioner implant has some code depends on this repo, we cannot avoid infection chain at all.

# Syntax concrete (3).
* load is omitted in root context.
* * name : (uri|path.path...)(.type|.function)?
* * import once, no need include guard.
* let is omitted in type and function context, like swift's let:
* * a : b ::
* * a : const(b) ::
* * * a is (non const) reference of b or const reference of b.
* * * the original variable will not be used like some awesome languages.
* * a : static(b) ::
* * * a is initialized by b once in first of execution.
* * a : auto(b) ::
* * a : type(b) ::
* * a : func(b) ::
* * * a is instanced object of b
* * a : func!(b) ::
* * * a is function returned reference.
* * a : (fn ...)(...) ... ::
* * * initialize with lambda function.
* * a : b : "comment" : "license"
* * * comment and license.
* * a : b :: \\n\\t ...
* * * variable block.
* def name : definition : "comment" : "license"
* * each operand isn't omittable.
* * block-wise.
* (inline|fn) name(name : type : "comment"?, ...) : type : "comment"? : "license"?
* * function, also lambda be. from awesome scala.
* * function name! returns reference.
* * type can specify (type \| ... \| type), also be able to specify template omitted.
* * with calling function, we must specify the variable name as: name(name = val, ...)
* * no return mnemonic, instead of them, reserved "res" variable.
* * * from awesome embedded C language.
* inverse(function, result) : worst case brute force inverse function.
* typeof, typeid
* type classname : inherit : "comment"? : "license"?
* * Only one class inheritation is accepted. Treat as arithmetic ones.
* * a +=item val
* * * operator with some extension to C.
* * * super class operator is also operator +=item .
* * * if there's no such operator, search super, then, replace with
* * * root class definition on op += or some with refactored one.
* * no private/protected, public only.
* * friend
* * * in-type friend functions to write down inter-type initializer operator.
* * ctor, dtor
* * * from some awesome programming languages.
* * enter, leave
* * * every variable change, we call this first / last on all of the functions in type.
* * only variable/def/ctor/dtor/friend/enter/leave/operator can be defined in type block. only operator we can specify return type others auto generated with this or void, we can specify leaf explicitly in another case.
* * needs super function explicit to call super functions, not to be auto called.
* * this and leaf (end of leaf of the object) and super object references.
* special types and variables and flows.
* * Int\[0\] : void, Object root, no inheritation.
* * Int\[\_\_pointer_bits\_\_\] inherites Int\[0\]. : only integer class, bool for nonzero.
* * \_\_pointer_bits\_\_
* * * number of pointer bit size.
* * \_\_here\_\_
* * * information for debugging.
* * \_\_callgraph\_\_
* * * information for debugging.
* * () ? () : ()
* * * 3-term operator like in C.
* * 'for ... from ... to ... : label', type.op ++for, type.op \<for function definition is used,
* * * break label
* * assert(x)
* * * stop execution or compilation if x is false.
* * * we can override this with assert function redefinition in execution time.
* \# will treat line after them as a comment:

# General Tips
If the data is enough, machine learning methods can implement any of the implementation.
For this, please refer randtools (with F_p integer, using (F_p)^k register on each).
So around this, we aims and we need the implementation of compact and low complexity and whole readable library, (and system).

# Tips
One of the method reducing source code is to exclude same or similar structures from reading binary perspective.
So with this, this repository aims and follows class capsule with arithmetic method in mathematics. One of a perspective of mathematics is to treat quantities and their structures as calculatible possible simple way, and is often has a structure repeat, invert, and they sometimes treated as arithmetic operations.
Some conditions needs variables along with initialize / ensure method.
Except some of the special flows especially initialize/error flow, I don't know whether or not special flows other than if then else, for, while is needed. And if we implement such flows with class capsule, it usually vanishes the condition.
But, it would be greately appreciated if you know the methods reducing source code other than along with preparing data nor how to treat data in the codes.

# Tips
After strongly optimized program, any m-output n-input program can be described as O(mn\*f(L)) arithmetic calculation time, and for parallel computing, O(f(L)), which L can be O(lg(n)) if we describe m, n as a bit. So we only focus on simplicity itself strong. (However, compiletime speed is very heavy.)

# Obscure tips
Thus above, if the program description only needs linear to memory size phenomenon, there's also apprehension to collision of algorithms they also caused by compile alike machine learning phenomenon. We might avoid this by selection of the phenomenon, however, in totally, we cannot. So in such case, we can try to compile them with small enough parts and collection of them nor compile before each compute on same machine, but even in their case, it is obscure it is valid or not on real raw meaning.
-&gt; This isn't critical because the invariant has multiple of the calculation
operand real value patterns as a one expression.
However, if there's a glitches s.t. multiple invariants into unsplittable
one dimension causes glitches on the computers.

# Tips on optimizing
We should use fractional type with sqrt-added field on the optimization
calculation. Also, we need some of the large accuracy on them.
So even we optimize 64-bit 2-term operations, we need many of the time.
So, we need divide and conquer method for general complemented calculations.
In C program with no memory reference, we only need 2 term optimizers, and
connect the first digit matrices block accurate method, then,
we get the last large matrix.
Otherwise, if some of the 3 or more term operators hardly depends on the
each of the operand, we need to optimize them as a whole.

# Things undone.
This repository has NO materialized implementation.
There exists huge fields they have important materials.

# Control directory
We cram the scrapped delusion which all the things we want supported by computer for now into Control directory (can be different from yours). Some of the future, this categorize can be changed.

